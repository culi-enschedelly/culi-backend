CREATE TABLE `users` (
    `uuid` VARCHAR(36) PRIMARY KEY
);

CREATE TABLE `recipes` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `subtitle` VARCHAR(255),
    `description` TEXT NOT NULL,
    `active_prep_time` INT NOT NULL,
    `passive_prep_time` INT,
    `is_full_recipe` BOOLEAN DEFAULT 1,
    `author_uuid` VARCHAR(36),
    `image_url` VARCHAR(511),

    FOREIGN KEY (author_uuid) REFERENCES users(uuid)
);

CREATE TABLE `steps` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `step` INT NOT NULL,
    `description` TEXT NOT NULL,
    `recipe_id` BIGINT(20) UNSIGNED NOT NULL,

    FOREIGN KEY (recipe_id) REFERENCES recipes(id)
);

CREATE TABLE `variations` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `description` TEXT NOT NULL,
    `recipe_id` BIGINT(20) UNSIGNED NOT NULL,

    FOREIGN KEY (recipe_id) REFERENCES recipes(id)
);

CREATE TABLE `ingredients` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE `ingredient_units` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `unit` VARCHAR(20) UNIQUE NOT NULL
);

INSERT INTO `ingredient_units` (`unit`) VALUES ('gr'), ('ml'), ('stuks'), ('snufje');

CREATE TABLE `ingredient_amounts` (
    `recipe_id` BIGINT(20) UNSIGNED NOT NULL,
    `ingredient_id` BIGINT(20) UNSIGNED NOT NULL,
    `amount` INT NOT NULL,
    `unit_id` BIGINT(20) UNSIGNED NOT NULL,

    PRIMARY KEY (recipe_id, ingredient_id),
    FOREIGN KEY (recipe_id) REFERENCES recipes(id),
    FOREIGN KEY (ingredient_id) REFERENCES ingredients(id),
    FOREIGN KEY (unit_id) REFERENCES ingredient_units(id)
);

CREATE TABLE `tag_types` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL
);

CREATE TABLE `tags` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `type_id` BIGINT(20) UNSIGNED,

    FOREIGN KEY (type_id) REFERENCES tag_types(id)
);

CREATE TABLE `recipe_tags` (
    `recipe_id` BIGINT(20) UNSIGNED NOT NULL,
    `tag_id` BIGINT(20) UNSIGNED NOT NULL,

    PRIMARY KEY (recipe_id, tag_id),
    FOREIGN KEY (recipe_id) REFERENCES recipes(id),
    FOREIGN KEY (tag_id) REFERENCES tags(id)
);

CREATE TABLE `equipments` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL
);

CREATE TABLE `recipe_equipments` (
    `recipe_id` BIGINT(20) UNSIGNED NOT NULL,
    `equipment_id` BIGINT(20) UNSIGNED NOT NULL,

    PRIMARY KEY (recipe_id, equipment_id),
    FOREIGN KEY (recipe_id) REFERENCES recipes(id),
    FOREIGN KEY (equipment_id) REFERENCES equipments(id)
);
