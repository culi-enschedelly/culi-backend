package nl.enschedelly.culi.repository;

import nl.enschedelly.culi.model.Recipe;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {

    @Query(value = "SELECT * FROM recipes WHERE CONCAT(name, ' ', subtitle, ' ', description) LIKE %?1%", nativeQuery = true)
    List<Recipe> findByQuery(String query);

    List<Recipe> findAll(Specification<Recipe> spec);
}
