package nl.enschedelly.culi.repository;

import nl.enschedelly.culi.model.IngredientAmount;
import org.springframework.data.repository.CrudRepository;

public interface IngredientAmountRepository extends CrudRepository<IngredientAmount, IngredientAmount.IngredientAmountId> {
}
