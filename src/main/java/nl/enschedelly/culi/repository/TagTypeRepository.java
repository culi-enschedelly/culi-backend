package nl.enschedelly.culi.repository;

import nl.enschedelly.culi.model.TagType;
import org.springframework.data.repository.CrudRepository;

public interface TagTypeRepository extends CrudRepository<TagType, Long> {

    boolean existsByName(String name);

    boolean existsByNameAndIdNot(String name, Long id);
}
