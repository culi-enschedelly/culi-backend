package nl.enschedelly.culi.repository;

import nl.enschedelly.culi.model.Variation;
import org.springframework.data.repository.CrudRepository;

public interface VariationRepository extends CrudRepository<Variation, Long> {
}
