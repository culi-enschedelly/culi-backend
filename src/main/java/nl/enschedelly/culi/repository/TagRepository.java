package nl.enschedelly.culi.repository;

import nl.enschedelly.culi.model.Tag;
import org.springframework.data.repository.CrudRepository;

public interface TagRepository extends CrudRepository<Tag, Long> {

    boolean existsByNameAndTypeId(String name, Long typeId);

    boolean existsByNameAndTypeIdAndIdNot(String name, Long typeId, Long id);
}
