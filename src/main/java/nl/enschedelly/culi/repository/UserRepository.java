package nl.enschedelly.culi.repository;

import nl.enschedelly.culi.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    boolean existsByUuid(String uuid);

    Optional<User> findByUuid(String uuid);

    void deleteByUuid(String uuid);
}

