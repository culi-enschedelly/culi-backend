package nl.enschedelly.culi.repository;

import nl.enschedelly.culi.model.IngredientUnit;
import org.springframework.data.repository.CrudRepository;

public interface IngredientUnitRepository extends CrudRepository<IngredientUnit, Long> {
}
