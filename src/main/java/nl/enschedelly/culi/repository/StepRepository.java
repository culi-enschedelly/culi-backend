package nl.enschedelly.culi.repository;

import nl.enschedelly.culi.model.Step;
import org.springframework.data.repository.CrudRepository;

public interface StepRepository extends CrudRepository<Step, Long> {
}
