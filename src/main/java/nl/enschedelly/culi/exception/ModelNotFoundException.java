package nl.enschedelly.culi.exception;

import org.springframework.http.HttpStatus;

public class ModelNotFoundException extends ResponseException {

    public ModelNotFoundException(String model) {
        super(HttpStatus.NOT_FOUND, String.format("The %s with the specified ID could not be found.", model));
    }
}
