package nl.enschedelly.culi.exception;

import org.springframework.http.HttpStatus;

public class AlreadyExistsException extends ResponseException {

    public AlreadyExistsException(String model) {
        super(HttpStatus.CONFLICT, String.format("The %s already exists.", model));
    }
}
