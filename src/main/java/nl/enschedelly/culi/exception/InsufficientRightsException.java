package nl.enschedelly.culi.exception;

import org.springframework.http.HttpStatus;

public class InsufficientRightsException extends ResponseException {

    public InsufficientRightsException() {
        super(HttpStatus.FORBIDDEN, "You do not have permission to perform this action");
    }
}
