package nl.enschedelly.culi.exception;

import org.springframework.http.HttpStatus;

public class EnschedellyUsersUnavailable extends ResponseException {

    public EnschedellyUsersUnavailable() {
        super(HttpStatus.SERVICE_UNAVAILABLE, "User service is unavailable");
    }
}
