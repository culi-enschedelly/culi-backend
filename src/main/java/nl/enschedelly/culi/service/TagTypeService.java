package nl.enschedelly.culi.service;

import nl.enschedelly.culi.dto.request.TagTypeRequestDto;
import nl.enschedelly.culi.dto.response.TagTypeResponseDto;
import nl.enschedelly.culi.dto.response.TagsByTypeResponseDto;
import nl.enschedelly.culi.exception.AlreadyExistsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.model.TagType;
import nl.enschedelly.culi.repository.TagTypeRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagTypeService {

    protected static final String MODEL_NAME = "tag type";

    @Autowired
    private TagTypeRepository repository;

    public List<TagTypeResponseDto> getAllTagTypes() {
        return Streamable.of(repository.findAll())
            .map(TagTypeResponseDto::from)
            .toList();
    }

    public List<TagsByTypeResponseDto> getAllTagsByType() {
        return Streamable.of(repository.findAll())
            .map(TagsByTypeResponseDto::from)
            .toList();
    }

    public TagTypeResponseDto getTagType(Long id) throws ModelNotFoundException {
        return repository.findById(id)
            .map(TagTypeResponseDto::from)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));
    }

    public TagTypeResponseDto createTagType(TagTypeRequestDto request) throws AlreadyExistsException {
        if (repository.existsByName(request.getName())) {
            throw new AlreadyExistsException(MODEL_NAME);
        }

        TagType tagType = new TagType()
            .setName(StringUtils.capitalize(request.getName().toLowerCase()));

        return TagTypeResponseDto.from(
            repository.save(tagType)
        );
    }

    public TagTypeResponseDto updateTagType(Long id, TagTypeRequestDto request) throws ModelNotFoundException, AlreadyExistsException {
        TagType tagType = repository.findById(id)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));

        if (repository.existsByNameAndIdNot(request.getName(), id)) {
            throw new AlreadyExistsException(MODEL_NAME);
        }

        tagType.setName(StringUtils.capitalize(request.getName().toLowerCase()));
        return TagTypeResponseDto.from(
            repository.save(tagType)
        );
    }

    public void deleteTagType(Long id) throws ModelNotFoundException {
        if (!repository.existsById(id)) {
            throw new ModelNotFoundException(MODEL_NAME);
        }
        repository.deleteById(id);
    }
}
