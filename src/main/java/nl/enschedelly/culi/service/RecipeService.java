package nl.enschedelly.culi.service;

import nl.enschedelly.culi.dto.request.RecipeRequestDto;
import nl.enschedelly.culi.dto.response.RecipeResponseDto;
import nl.enschedelly.culi.exception.InsufficientRightsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.model.*;
import nl.enschedelly.culi.repository.*;
import nl.enschedelly.culi.security.AuthUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RecipeService extends BaseService {

    private static final String MODEL_NAME = "recipe";

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private IngredientUnitRepository ingredientUnitRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private EquipmentRepository equipmentRepository;

    @Autowired
    private VariationRepository variationRepository;

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private IngredientAmountRepository ingredientAmountRepository;

    // TODO: used for debug purposes now
    @Autowired
    private UserRepository userRepository;

    public List<RecipeResponseDto> getAllRecipes(
        Integer maxFullPrepTime,
        Integer maxActivePrepTime,
        Long[] tagIds
    ) {
        Specification<Recipe> spec = getRecipeSpecification(maxFullPrepTime, maxActivePrepTime, tagIds);
        return Streamable.of(recipeRepository.findAll(spec))
            .map(RecipeResponseDto::from)
            .toList();
    }

    public List<RecipeResponseDto> searchRecipes(String query) {
        return recipeRepository.findByQuery(query)
            .stream().map(RecipeResponseDto::from)
            .collect(Collectors.toList());
    }

    public RecipeResponseDto getRecipe(Long id) throws ModelNotFoundException {
        return recipeRepository.findById(id)
            .map(RecipeResponseDto::from)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));
    }

    @Transactional(rollbackFor = Exception.class)
    public RecipeResponseDto createRecipe(RecipeRequestDto request) throws ModelNotFoundException {
        Recipe recipe = recipeFromRequest(request);
        return RecipeResponseDto.from(recipe);
    }

    @Transactional(rollbackFor = Exception.class)
    public RecipeResponseDto updateRecipe(Long id, RecipeRequestDto request) throws ModelNotFoundException, InsufficientRightsException {
        Recipe recipe = recipeRepository.findById(id)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));

        if (!hasWriteAccessTo(recipe)) {
            throw new InsufficientRightsException();
        }

        recipe = recipeFromRequest(recipe, request);
        return RecipeResponseDto.from(recipe);
    }

    public void deleteRecipe(Long id) throws ModelNotFoundException, InsufficientRightsException {
        Recipe recipe = recipeRepository.findById(id)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));

        if (!hasWriteAccessTo(recipe)) {
            throw new InsufficientRightsException();
        }

        recipeRepository.deleteById(id);
    }

    private Recipe recipeFromRequest(Recipe recipe, RecipeRequestDto request) throws ModelNotFoundException {
        Set<Tag> tags = request.getTagIds()
            .stream()
            .map(t -> tagRepository.findById(t).orElse(null))
            .collect(Collectors.toSet());

        if (tags.contains(null)) {
            throw new ModelNotFoundException("tag");
        }

        Set<Equipment> equipments = request.getEquipmentIds()
            .stream()
            .map(e -> equipmentRepository.findById(e).orElse(null))
            .collect(Collectors.toSet());

        if (equipments.contains(null)) {
            throw new ModelNotFoundException("equipment");
        }

        // TODO: use actual user
        User author = userRepository.findAll().iterator().next();

        recipe
            .setName(request.getName())
            .setSubtitle(request.getSubtitle())
            .setDescription(request.getDescription())
            .setActivePrepTime(request.getActivePrepTime())
            .setPassivePrepTime(request.getPassivePrepTime())
            .setIsFullRecipe(request.getIsFullRecipe())
            .setAuthor(author)
            .setImageUrl(request.getImageUrl())
            .setTags(tags)
            .setEquipments(equipments);

        recipeRepository.save(recipe);

        Set<Variation> variations = request.getVariations()
            .stream()
            .map(v -> new Variation().setDescription(v.getDescription()).setRecipe(recipe))
            .collect(Collectors.toSet());

        Set<Step> steps = request.getSteps()
            .stream()
            .map(s -> new Step().setStep(s.getStep()).setDescription(s.getDescription()).setRecipe(recipe))
            .collect(Collectors.toSet());

        Set<IngredientAmount> ingredientAmounts = request.getIngredientAmounts()
            .stream()
            .map(i -> {
                if (!ingredientRepository.existsById(i.getIngredientId())) {
                    return null;
                }

                if (!ingredientUnitRepository.existsById(i.getUnitId())) {
                    return null;
                }
                return new IngredientAmount()
                    .setIngredientAmountId(new IngredientAmount.IngredientAmountId(recipe.getId(), i.getIngredientId()))
                    .setIngredient(ingredientRepository.findById(i.getIngredientId()).get())
                    .setRecipe(recipe)
                    .setAmount(i.getAmount())
                    .setUnitId(i.getUnitId())
                    // TODO: why is this necessary?
                    .setUnit(ingredientUnitRepository.findById(i.getUnitId()).get());
            })
            .collect(Collectors.toSet());

        if (ingredientAmounts.contains(null)) {
            throw new ModelNotFoundException("ingredient or unit");
        }

        variationRepository.saveAll(variations);
        stepRepository.saveAll(steps);
        ingredientAmountRepository.saveAll(ingredientAmounts);

        if (recipe.getVariations() != null) {
            recipe.getVariations().clear();
        } else {
            recipe.setVariations(new HashSet<>());
        }
        recipe.getVariations().addAll(variations);

        if (recipe.getSteps() != null) {
            recipe.getSteps().clear();
        } else {
            recipe.setSteps(new HashSet<>());
        }
        recipe.getSteps().addAll(steps);

        if (recipe.getIngredientAmounts() != null) {
            recipe.getIngredientAmounts().clear();
        } else {
            recipe.setIngredientAmounts(new HashSet<>());
        }
        recipe.getIngredientAmounts().addAll(ingredientAmounts);

        recipeRepository.save(recipe);

        return recipeRepository.findById(recipe.getId()).get();
    }

    private Recipe recipeFromRequest(RecipeRequestDto request) throws ModelNotFoundException {
        return recipeFromRequest(new Recipe(), request);
    }

    private Specification<Recipe> getRecipeSpecification(
            Integer maxFullPrepTime,
            Integer maxActivePrepTime,
            Long[] tagIds) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (maxFullPrepTime != null) {
                predicates.add(criteriaBuilder.le(criteriaBuilder.sum(root.get("activePrepTime"), root.get("passivePrepTime")), maxFullPrepTime));
            }

            if (maxActivePrepTime != null) {
                predicates.add(criteriaBuilder.le(root.get("activePrepTime"), maxActivePrepTime));
            }

            if (tagIds != null) {
                predicates.add(root.join("tags", JoinType.LEFT).get("id").in((Object[]) tagIds));
                query.groupBy(root.get("id")).having(
                    criteriaBuilder.equal(
                        criteriaBuilder.count(root.get("id")),
                        tagIds.length
                    )
                );
            }

            return criteriaBuilder.and(predicates.toArray(Predicate[]::new));
        };
    }

    private boolean hasWriteAccessTo(Recipe recipe) {
        AuthUser loggedInUser = getLoggedInUser();
        return loggedInUser.getIsAdmin() || recipe.getAuthor().getUuid().equals(loggedInUser.getUuid());
    }
}
