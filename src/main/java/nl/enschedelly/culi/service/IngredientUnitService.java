package nl.enschedelly.culi.service;

import nl.enschedelly.culi.dto.response.IngredientUnitResponseDto;
import nl.enschedelly.culi.repository.IngredientUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientUnitService {

    @Autowired
    private IngredientUnitRepository repository;

    public List<IngredientUnitResponseDto> getAllUnits() {
        return Streamable.of(repository.findAll())
            .map(IngredientUnitResponseDto::from)
            .toList();
    }
}
