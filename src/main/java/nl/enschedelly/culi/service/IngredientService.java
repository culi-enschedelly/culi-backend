package nl.enschedelly.culi.service;

import nl.enschedelly.culi.dto.request.IngredientRequestDto;
import nl.enschedelly.culi.dto.response.IngredientResponseDto;
import nl.enschedelly.culi.exception.AlreadyExistsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.model.Ingredient;
import nl.enschedelly.culi.repository.IngredientRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientService {

    private static final String MODEL_NAME = "ingredient";

    @Autowired
    private IngredientRepository repository;

    public List<IngredientResponseDto> getAllIngredients() {
        return Streamable.of(repository.findAll())
            .map(IngredientResponseDto::from)
            .toList();
    }

    public IngredientResponseDto getIngredient(Long id) throws ModelNotFoundException {
        return repository.findById(id)
            .map(IngredientResponseDto::from)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));
    }

    public IngredientResponseDto createIngredient(IngredientRequestDto request) throws AlreadyExistsException {
        if (repository.existsByName(request.getName())) {
            throw new AlreadyExistsException(MODEL_NAME);
        }

        Ingredient ingredient = new Ingredient()
            .setName(StringUtils.capitalize(request.getName().toLowerCase()));

        return IngredientResponseDto.from(
            repository.save(ingredient)
        );
    }

    public IngredientResponseDto updateIngredient(Long id, IngredientRequestDto request) throws AlreadyExistsException, ModelNotFoundException {
        Ingredient ingredient = repository.findById(id)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));

        if (repository.existsByNameAndIdNot(request.getName(), id)) {
            throw new AlreadyExistsException(MODEL_NAME);
        }

        ingredient.setName(StringUtils.capitalize(request.getName().toLowerCase()));
        return IngredientResponseDto.from(
            repository.save(ingredient)
        );
    }

    public void deleteIngredient(Long id) throws ModelNotFoundException {
        if(!repository.existsById(id)) {
            throw new ModelNotFoundException(MODEL_NAME);
        }
        repository.deleteById(id);
    }

}
