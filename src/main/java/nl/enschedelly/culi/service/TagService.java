package nl.enschedelly.culi.service;

import nl.enschedelly.culi.dto.request.TagRequestDto;
import nl.enschedelly.culi.dto.response.IngredientResponseDto;
import nl.enschedelly.culi.dto.response.TagResponseDto;
import nl.enschedelly.culi.dto.response.TagsByTypeResponseDto;
import nl.enschedelly.culi.exception.AlreadyExistsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.model.Tag;
import nl.enschedelly.culi.model.TagType;
import nl.enschedelly.culi.repository.TagRepository;
import nl.enschedelly.culi.repository.TagTypeRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {

    private static final String MODEL_NAME = "tag";

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private TagTypeRepository typeRepository;

    public List<TagResponseDto> getAllTags() {
        return Streamable.of(tagRepository.findAll())
            .map(TagResponseDto::from)
            .toList();
    }

    public TagResponseDto getTag(Long id) throws ModelNotFoundException {
        return tagRepository.findById(id)
            .map(TagResponseDto::from)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));
    }

    public TagResponseDto createTag(TagRequestDto request) throws AlreadyExistsException, ModelNotFoundException {
        if (tagRepository.existsByNameAndTypeId(request.getName(), request.getTypeId())) {
            throw new AlreadyExistsException(MODEL_NAME);
        }

        if (!typeRepository.existsById(request.getTypeId())) {
            throw new ModelNotFoundException(TagTypeService.MODEL_NAME);
        }

        TagType type = typeRepository.findById(request.getTypeId())
            .orElseThrow(() -> new ModelNotFoundException(TagTypeService.MODEL_NAME));

        Tag tag = new Tag()
            .setName(StringUtils.capitalize(request.getName().toLowerCase()))
            .setType(type);

        return TagResponseDto.from(
            tagRepository.save(tag)
        );
    }

    public TagResponseDto updateTag(Long id, TagRequestDto request) throws AlreadyExistsException, ModelNotFoundException {
        Tag tag = tagRepository.findById(id)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));

        if (!typeRepository.existsById(request.getTypeId())) {
            throw new ModelNotFoundException(TagTypeService.MODEL_NAME);
        }

        TagType type = typeRepository.findById(request.getTypeId())
            .orElseThrow(() -> new ModelNotFoundException(TagTypeService.MODEL_NAME));

        tag.setName(StringUtils.capitalize(request.getName().toLowerCase()))
            .setType(type);
        return TagResponseDto.from(
            tagRepository.save(tag)
        );
    }

    public void deleteTag(Long id) throws ModelNotFoundException {
        if (!tagRepository.existsById(id)) {
            throw new ModelNotFoundException(MODEL_NAME);
        }
        tagRepository.deleteById(id);
    }

}
