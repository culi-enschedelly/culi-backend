package nl.enschedelly.culi.service;

import nl.enschedelly.culi.dto.request.EquipmentRequestDto;
import nl.enschedelly.culi.dto.response.EquipmentResponseDto;
import nl.enschedelly.culi.exception.AlreadyExistsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.model.Equipment;
import nl.enschedelly.culi.repository.EquipmentRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipmentService {

    private static final String MODEL_NAME = "equipment";

    @Autowired
    private EquipmentRepository repository;

    public List<EquipmentResponseDto> getAllEquipments() {
        return Streamable.of(repository.findAll())
            .map(EquipmentResponseDto::from)
            .toList();
    }

    public EquipmentResponseDto getEquipment(Long id) throws ModelNotFoundException {
        return repository.findById(id)
            .map(EquipmentResponseDto::from)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));
    }

    public EquipmentResponseDto createEquipment(EquipmentRequestDto request) throws AlreadyExistsException {
        if (repository.existsByName(request.getName())) {
            throw new AlreadyExistsException(MODEL_NAME);
        }

        Equipment equipment = new Equipment()
            .setName(StringUtils.capitalize(request.getName().toLowerCase()));

        return EquipmentResponseDto.from(
            repository.save(equipment)
        );
    }

    public EquipmentResponseDto updateEquipment(Long id, EquipmentRequestDto request) throws ModelNotFoundException, AlreadyExistsException {
        Equipment equipment = repository.findById(id)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));

        if (repository.existsByNameAndIdNot(request.getName(), id)) {
            throw new AlreadyExistsException(MODEL_NAME);
        }

        equipment.setName(StringUtils.capitalize(request.getName().toLowerCase()));
        return EquipmentResponseDto.from(
            repository.save(equipment)
        );
    }

    public void deleteEquipment(Long id) throws ModelNotFoundException {
        if (!repository.existsById(id)) {
            throw new ModelNotFoundException(MODEL_NAME);
        }
        repository.deleteById(id);
    }
}
