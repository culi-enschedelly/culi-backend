package nl.enschedelly.culi.service;

import nl.enschedelly.culi.security.AuthUser;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class BaseService {

    public AuthUser getLoggedInUser() {
        return (AuthUser) ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getAttribute("user");
    }
}
