package nl.enschedelly.culi.command;

import nl.enschedelly.culi.model.Equipment;
import nl.enschedelly.culi.model.Ingredient;
import nl.enschedelly.culi.model.Tag;
import nl.enschedelly.culi.model.TagType;
import nl.enschedelly.culi.repository.EquipmentRepository;
import nl.enschedelly.culi.repository.IngredientRepository;
import nl.enschedelly.culi.repository.TagRepository;
import nl.enschedelly.culi.repository.TagTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.Arrays;

@ShellComponent
public class SeedDatabaseCommand {

    @Autowired
    private TagTypeRepository tagTypeRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private EquipmentRepository equipmentRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @ShellMethod("Seed the database")
    public String seedDb() {
        TagType country = new TagType().setName("Country");
        TagType course = new TagType().setName("Course");
        TagType difficulty = new TagType().setName("Difficulty");

        tagTypeRepository.saveAll(
            Arrays.asList(country, course, difficulty)
        );

        tagRepository.saveAll(
            Arrays.asList(
                new Tag().setName("Dutch").setType(country),
                new Tag().setName("Italian").setType(country),
                new Tag().setName("Appetizer").setType(course),
                new Tag().setName("Main").setType(course),
                new Tag().setName("⭐️").setType(difficulty),
                new Tag().setName("⭐⭐⭐⭐⭐️").setType(difficulty)
            )
        );

        equipmentRepository.saveAll(
            Arrays.asList(
                new Equipment().setName("Oven"),
                new Equipment().setName("Microwave"),
                new Equipment().setName("Pastamachine"),
                new Equipment().setName("Raps")
            )
        );

        ingredientRepository.saveAll(
            Arrays.asList(
                new Ingredient().setName("Diepvriespizza"),
                new Ingredient().setName("Kaas"),
                new Ingredient().setName("Gehaktbal")
            )
        );

        return "Database successfully seeded";
    }
}
