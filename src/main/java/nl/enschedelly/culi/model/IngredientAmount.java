package nl.enschedelly.culi.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "ingredient_amounts")
public class IngredientAmount {

    @EmbeddedId
    private IngredientAmountId ingredientAmountId;

    @ManyToOne
    @MapsId("recipe_id")
    @JoinColumn(name = "recipe_id", nullable = false)
    private Recipe recipe;

    @ManyToOne
    @MapsId("ingredient_id")
    @JoinColumn(name = "ingredient_id", nullable = false, insertable = false, updatable = false)
    private Ingredient ingredient;

    private Integer amount;

    @Column(name = "unit_id")
    private Long unitId;

    @ManyToOne
    @JoinColumn(name = "unit_id", nullable = false, insertable = false, updatable = false)
    private IngredientUnit unit;

    @Getter
    @Setter
    @Embeddable
    public static class IngredientAmountId implements Serializable {
        @Column(name = "recipe_id", nullable = false)
        private Long recipeId;

        @Column(name = "ingredient_id", nullable = false)
        private Long ingredientId;

        public IngredientAmountId(Long recipeId, Long ingredientId) {
            this.recipeId = recipeId;
            this.ingredientId = ingredientId;
        }

        public IngredientAmountId() {}

        @Override
        public boolean equals(Object other) {
            if (this == other) return true;
            if (!(other instanceof IngredientAmountId)) return false;
            IngredientAmountId otherIngredientAmountId = (IngredientAmountId) other;
            return this.recipeId.equals(otherIngredientAmountId.getRecipeId()) &&
                this.ingredientId.equals(otherIngredientAmountId.getIngredientId());
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.recipeId, this.ingredientId);
        }
    }
}
