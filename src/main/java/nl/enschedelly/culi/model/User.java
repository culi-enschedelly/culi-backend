package nl.enschedelly.culi.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "users")
public class User { // implements Serializable --> causes imports to fail!

    @Id
    private String uuid;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "author")
    private Set<Recipe> recipes;
}
