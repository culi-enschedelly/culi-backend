package nl.enschedelly.culi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuliBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(CuliBackendApplication.class, args);
    }

}
