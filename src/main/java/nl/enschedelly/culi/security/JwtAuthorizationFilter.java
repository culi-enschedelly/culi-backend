package nl.enschedelly.culi.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.PublicKey;

public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final PublicKey jwtVerifyingKey;

    public JwtAuthorizationFilter(PublicKey jwtVerifyingKey) {
        this.jwtVerifyingKey = jwtVerifyingKey;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authHeader = request.getHeader("X-Authorization");
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            chain.doFilter(request, response);
            return;
        }

        String token = authHeader.substring("Bearer ".length());
        Jws<Claims> jws;
        try {
            jws = Jwts.parserBuilder()
                .setSigningKey(jwtVerifyingKey)
                .build()
                .parseClaimsJws(token);
        } catch (JwtException e) {
            chain.doFilter(request, response);
            return;
        }

        AuthUser user = new ObjectMapper().convertValue(jws.getBody().get("user"), AuthUser.class);
        request.setAttribute("user", user);
        chain.doFilter(request, response);
    }
}
