package nl.enschedelly.culi.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.security.PublicKey;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PublicKey jwtVerifyingKey;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .formLogin().disable()
            .httpBasic().disable()
            .logout().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
            .addFilterAfter(new JwtAuthorizationFilter(jwtVerifyingKey), UsernamePasswordAuthenticationFilter.class)
            .authorizeRequests()
            .anyRequest().permitAll();
    }
}
