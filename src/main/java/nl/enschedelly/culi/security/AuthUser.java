package nl.enschedelly.culi.security;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthUser {

    private String uuid;

    private String firstName;

    private String lastName;

    private String email;

    private Long joinedAt;

    private Boolean isAdmin;
}
