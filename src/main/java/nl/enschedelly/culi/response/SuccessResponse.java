package nl.enschedelly.culi.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuccessResponse<T> extends Response {

    private T data;

    public SuccessResponse(T data) {
        super(true);
        this.data = data;
    }
}
