package nl.enschedelly.culi.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Response {

    private boolean success;

    public Response(boolean success) {
        this.success = success;
    }

    public Response() {}
}
