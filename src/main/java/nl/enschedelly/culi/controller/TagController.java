package nl.enschedelly.culi.controller;

import nl.enschedelly.culi.dto.request.TagRequestDto;
import nl.enschedelly.culi.dto.response.TagResponseDto;
import nl.enschedelly.culi.dto.response.TagsByTypeResponseDto;
import nl.enschedelly.culi.exception.AlreadyExistsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.security.RequiresAuthentication;
import nl.enschedelly.culi.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tags")
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping("")
    public List<TagResponseDto> getAllTags() {
        return tagService.getAllTags();
    }

    @GetMapping("/{id}")
    public TagResponseDto getTag(@PathVariable Long id) throws ModelNotFoundException {
        return tagService.getTag(id);
    }

    @RequiresAuthentication
    @PostMapping("")
    public TagResponseDto createTag(@Valid @RequestBody TagRequestDto request) throws AlreadyExistsException, ModelNotFoundException {
        return tagService.createTag(request);
    }

    @RequiresAuthentication
    @PutMapping("/{id}")
    public TagResponseDto updateTag(@PathVariable Long id, @Valid @RequestBody TagRequestDto request)
                          throws AlreadyExistsException, ModelNotFoundException {
        return tagService.updateTag(id, request);
    }

    @RequiresAuthentication
    @DeleteMapping("/{id}")
    public void deleteTag(@PathVariable Long id) throws ModelNotFoundException {
        tagService.deleteTag(id);
    }
}
