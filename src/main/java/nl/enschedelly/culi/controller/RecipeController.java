package nl.enschedelly.culi.controller;

import nl.enschedelly.culi.dto.request.RecipeRequestDto;
import nl.enschedelly.culi.dto.response.RecipeResponseDto;
import nl.enschedelly.culi.exception.InsufficientRightsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.security.RequiresAuthentication;
import nl.enschedelly.culi.security.AuthUser;
import nl.enschedelly.culi.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/recipes")
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @GetMapping("")
    public List<RecipeResponseDto> getAllRecipes(
            @RequestParam(value = "maxFullPrepTime", required = false) Integer maxFullPrepTime,
            @RequestParam(value = "maxActivePrepTime", required = false) Integer maxActivePrepTime,
            @RequestParam(value = "tagIds", required = false) Long[] tagIds
        ) {
        return recipeService.getAllRecipes(
            maxFullPrepTime,
            maxActivePrepTime,
            tagIds
        );
    }

    @GetMapping("/search")
    public List<RecipeResponseDto> searchRecipes(@RequestParam("q") String query) {
        return recipeService.searchRecipes(query);
    }

    @GetMapping("/{id}")
    public RecipeResponseDto getRecipe(@PathVariable Long id) throws ModelNotFoundException {
        return recipeService.getRecipe(id);
    }

    @RequiresAuthentication
    @PostMapping("")
    public RecipeResponseDto createRecipe(@Valid @RequestBody RecipeRequestDto request) throws ModelNotFoundException {
        return recipeService.createRecipe(request);
    }

    @RequiresAuthentication
    @PutMapping("/{id}")
    public RecipeResponseDto updateRecipe(
        @PathVariable Long id,
        @Valid @RequestBody RecipeRequestDto request
    ) throws ModelNotFoundException, InsufficientRightsException {
        return recipeService.updateRecipe(id, request);
    }

    @RequiresAuthentication
    @DeleteMapping("/{id}")
    public void deleteRecipe(@PathVariable Long id) throws ModelNotFoundException, InsufficientRightsException {
        recipeService.deleteRecipe(id);
    }
}
