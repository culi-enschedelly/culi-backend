package nl.enschedelly.culi.controller;

import nl.enschedelly.culi.dto.request.IngredientRequestDto;
import nl.enschedelly.culi.dto.response.IngredientResponseDto;
import nl.enschedelly.culi.exception.AlreadyExistsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.security.RequiresAuthentication;
import nl.enschedelly.culi.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ingredients")
public class IngredientController {

    @Autowired
    private IngredientService ingredientService;

    @GetMapping("")
    public List<IngredientResponseDto> getAllIngredients() {
        return ingredientService.getAllIngredients();
    }

    @GetMapping("/{id}")
    public IngredientResponseDto getIngredient(@PathVariable Long id) throws ModelNotFoundException {
        return ingredientService.getIngredient(id);
    }

    @RequiresAuthentication
    @PostMapping("")
    public IngredientResponseDto createIngredient(@Valid @RequestBody IngredientRequestDto request) throws AlreadyExistsException {
        return ingredientService.createIngredient(request);
    }

    @RequiresAuthentication
    @PutMapping("/{id}")
    public IngredientResponseDto updateIngredient(@PathVariable Long id, @Valid @RequestBody IngredientRequestDto request)
                                 throws AlreadyExistsException, ModelNotFoundException {
        return ingredientService.updateIngredient(id, request);
    }

    @RequiresAuthentication
    @DeleteMapping("/{id}")
    public void deleteIngredient(@PathVariable Long id) throws ModelNotFoundException {
        ingredientService.deleteIngredient(id);
    }
}
