package nl.enschedelly.culi.controller;

import nl.enschedelly.culi.dto.request.TagTypeRequestDto;
import nl.enschedelly.culi.dto.response.TagTypeResponseDto;
import nl.enschedelly.culi.dto.response.TagsByTypeResponseDto;
import nl.enschedelly.culi.exception.AlreadyExistsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.security.RequiresAuthentication;
import nl.enschedelly.culi.service.TagTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tag-types")
public class TagTypeController {

    @Autowired
    private TagTypeService tagTypeService;

    @GetMapping("")
    public List<TagTypeResponseDto> getAllTagTypes() {
        return tagTypeService.getAllTagTypes();
    }

    @GetMapping("/with-tags")
    public List<TagsByTypeResponseDto> getAllTagsByType() {
        return tagTypeService.getAllTagsByType();
    }

    @GetMapping("/{id}")
    public TagTypeResponseDto getTagType(@PathVariable Long id) throws ModelNotFoundException {
        return tagTypeService.getTagType(id);
    }

    @RequiresAuthentication
    @PostMapping("")
    public TagTypeResponseDto createTagType(@Valid @RequestBody TagTypeRequestDto tagTypeRequestDto) throws AlreadyExistsException {
        return tagTypeService.createTagType(tagTypeRequestDto);
    }

    @RequiresAuthentication
    @PutMapping("/{id}")
    public TagTypeResponseDto updateTagType(@PathVariable Long id, @Valid @RequestBody TagTypeRequestDto tagTypeRequestDto)
                                            throws ModelNotFoundException, AlreadyExistsException {
        return tagTypeService.updateTagType(id, tagTypeRequestDto);
    }

    @RequiresAuthentication
    @DeleteMapping("/{id}")
    public void deleteTagType(@PathVariable Long id) throws ModelNotFoundException {
        tagTypeService.deleteTagType(id);
    }
}
