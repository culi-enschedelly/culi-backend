package nl.enschedelly.culi.controller;

import nl.enschedelly.culi.dto.response.IngredientUnitResponseDto;
import nl.enschedelly.culi.service.IngredientUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/units")
public class IngredientUnitsController {

    @Autowired
    private IngredientUnitService unitService;

    @GetMapping("")
    public List<IngredientUnitResponseDto> getAllUnits() {
        return unitService.getAllUnits();
    }
}
