package nl.enschedelly.culi.controller;

import nl.enschedelly.culi.dto.request.EquipmentRequestDto;
import nl.enschedelly.culi.dto.response.EquipmentResponseDto;
import nl.enschedelly.culi.exception.AlreadyExistsException;
import nl.enschedelly.culi.exception.ModelNotFoundException;
import nl.enschedelly.culi.security.RequiresAdmin;
import nl.enschedelly.culi.security.RequiresAuthentication;
import nl.enschedelly.culi.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/equipments")
public class EquipmentController {

    @Autowired
    private EquipmentService equipmentService;

    @GetMapping("")
    public List<EquipmentResponseDto> getAllEquipments() {
        return equipmentService.getAllEquipments();
    }

    @GetMapping("/{id}")
    public EquipmentResponseDto getEquipment(@PathVariable Long id) throws ModelNotFoundException {
        return equipmentService.getEquipment(id);
    }

    @RequiresAuthentication
    @PostMapping("")
    public EquipmentResponseDto createEquipment(@Valid @RequestBody EquipmentRequestDto equipmentRequestDto) throws AlreadyExistsException {
        return equipmentService.createEquipment(equipmentRequestDto);
    }

    @RequiresAuthentication
    @PutMapping("/{id}")
    public EquipmentResponseDto updateEquipment(@PathVariable Long id, @Valid @RequestBody EquipmentRequestDto equipmentRequestDto)
                                                throws ModelNotFoundException, AlreadyExistsException {
        return equipmentService.updateEquipment(id, equipmentRequestDto);
    }

    @RequiresAuthentication
    @DeleteMapping("/{id}")
    public void deleteEquipment(@PathVariable Long id) throws ModelNotFoundException {
        equipmentService.deleteEquipment(id);
    }
}
