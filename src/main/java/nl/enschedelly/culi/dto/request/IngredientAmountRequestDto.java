package nl.enschedelly.culi.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class IngredientAmountRequestDto {

    private Long ingredientId;

    private Integer amount;

    private Long unitId;

    public IngredientAmountRequestDto(Long ingredientId, Integer amount, Long unitId) {
        this.ingredientId = ingredientId;
        this.amount = amount;
        this.unitId = unitId;
    }
}
