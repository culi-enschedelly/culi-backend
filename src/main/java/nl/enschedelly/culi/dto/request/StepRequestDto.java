package nl.enschedelly.culi.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class StepRequestDto {

    @NotNull
    private Integer step;

    @NotNull
    private String description;
}
