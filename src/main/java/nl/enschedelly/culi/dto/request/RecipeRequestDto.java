package nl.enschedelly.culi.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecipeRequestDto {

    @NotNull
    private String name;

    private String subtitle;

    @NotNull
    private String description;

    @NotNull
    private Integer activePrepTime;

    private Integer passivePrepTime;

    @NotNull
    private Boolean isFullRecipe;

    private String imageUrl;

    @NotNull
    private List<VariationRequestDto> variations;

    @NotNull
    @Size(min = 1)
    private List<StepRequestDto> steps;

    @NotNull
    @Size(min = 1)
    private List<IngredientAmountRequestDto> ingredientAmounts;

    @NotNull
    private List<Long> tagIds;

    @NotNull
    private List<Long> equipmentIds;
}
