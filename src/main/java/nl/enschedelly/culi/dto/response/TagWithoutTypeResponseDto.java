package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.Tag;

@Getter
@Setter
public class TagWithoutTypeResponseDto {

    private Long id;

    private String name;

    public TagWithoutTypeResponseDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public TagWithoutTypeResponseDto() {}

    public static TagWithoutTypeResponseDto from(Tag tag) {
        return new TagWithoutTypeResponseDto(tag.getId(), tag.getName());
    }
}
