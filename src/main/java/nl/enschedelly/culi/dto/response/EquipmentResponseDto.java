package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.Equipment;

@Getter
@Setter
public class EquipmentResponseDto {

    private Long id;

    private String name;

    public EquipmentResponseDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public EquipmentResponseDto() {}

    public static EquipmentResponseDto from(Equipment equipment) {
        return new EquipmentResponseDto(equipment.getId(), equipment.getName());
    }
}
