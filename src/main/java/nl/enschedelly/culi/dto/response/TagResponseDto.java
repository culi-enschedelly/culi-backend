package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.Tag;

@Getter
@Setter
public class TagResponseDto {

    private Long id;

    private String name;

    private TagTypeResponseDto type;

    public TagResponseDto(Long id, String name, TagTypeResponseDto type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public TagResponseDto() {}

    public static TagResponseDto from(Tag tag) {
        return new TagResponseDto(tag.getId(), tag.getName(), TagTypeResponseDto.from(tag.getType()));
    }
}
