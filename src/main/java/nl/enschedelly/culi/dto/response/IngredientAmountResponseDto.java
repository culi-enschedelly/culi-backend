package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.IngredientAmount;

@Getter
@Setter
public class IngredientAmountResponseDto {

    private IngredientResponseDto ingredient;

    private Integer amount;

    private IngredientUnitResponseDto unit;

    public IngredientAmountResponseDto(IngredientResponseDto ingredientResponseDto, Integer amount, IngredientUnitResponseDto unit) {
        this.ingredient = ingredientResponseDto;
        this.amount = amount;
        this.unit = unit;
    }

    public IngredientAmountResponseDto() {}

    public static IngredientAmountResponseDto from(IngredientAmount ingredientAmount) {
        return new IngredientAmountResponseDto(
            IngredientResponseDto.from(ingredientAmount.getIngredient()),
            ingredientAmount.getAmount(),
            IngredientUnitResponseDto.from(ingredientAmount.getUnit())
        );
    }
}
