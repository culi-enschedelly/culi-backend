package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.TagType;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TagsByTypeResponseDto {

    private Long id;

    private String name;

    private List<TagWithoutTypeResponseDto> tags;

    public TagsByTypeResponseDto(Long id, String name, List<TagWithoutTypeResponseDto> tags) {
        this.id = id;
        this.name = name;
        this.tags = tags;
    }

    public TagsByTypeResponseDto() {}

    public static TagsByTypeResponseDto from(TagType tagType) {
        return new TagsByTypeResponseDto(
            tagType.getId(),
            tagType.getName(),
            tagType.getTags().stream().map(TagWithoutTypeResponseDto::from).collect(Collectors.toList())
        );
    }


}
