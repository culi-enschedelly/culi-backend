package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.TagType;

@Getter
@Setter
public class TagTypeResponseDto {

    private Long id;

    private String name;

    public TagTypeResponseDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public TagTypeResponseDto() {}

    public static TagTypeResponseDto from(TagType tagType) {
        return new TagTypeResponseDto(tagType.getId(), tagType.getName());
    }
}
