package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import nl.enschedelly.culi.model.*;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Accessors(chain = true)
public class RecipeResponseDto {

    private Long id;

    private String name;

    private String subtitle;

    private String description;

    private Integer activePrepTime;

    private Integer passivePrepTime;

    private Boolean isFullRecipe;

    // TODO: hide when necessary
    private String authorUuid;

    private String imageUrl;

    private List<VariationResponseDto> variations;

    private List<StepResponseDto> steps;

    private List<IngredientAmountResponseDto> ingredientAmounts;

    private List<TagResponseDto> tags;

    private List<EquipmentResponseDto> equipments;

    public static RecipeResponseDto from(Recipe recipe) {
        return new RecipeResponseDto()
            .setId(recipe.getId())
            .setName(recipe.getName())
            .setSubtitle(recipe.getSubtitle())
            .setDescription(recipe.getDescription())
            .setActivePrepTime(recipe.getActivePrepTime())
            .setPassivePrepTime(recipe.getPassivePrepTime())
            .setIsFullRecipe(recipe.getIsFullRecipe())
            .setAuthorUuid(recipe.getAuthor().getUuid())
            .setImageUrl(recipe.getImageUrl())
            .setVariations(
                recipe.getVariations().stream().map(VariationResponseDto::from).collect(Collectors.toList())
            )
            .setSteps(
                recipe.getSteps().stream().map(StepResponseDto::from).collect(Collectors.toList())
            )
            .setIngredientAmounts(
                recipe.getIngredientAmounts().stream().map(IngredientAmountResponseDto::from).collect(Collectors.toList())
            )
            .setTags(
                recipe.getTags().stream().map(TagResponseDto::from).collect(Collectors.toList())
            )
            .setEquipments(
                recipe.getEquipments().stream().map(EquipmentResponseDto::from).collect(Collectors.toList())
            );
    }
}
