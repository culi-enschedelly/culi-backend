package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.Step;

@Getter
@Setter
public class StepResponseDto {

    private Long id;

    private Integer step;

    private String description;

    public StepResponseDto(Long id, Integer step, String description) {
        this.id = id;
        this.step = step;
        this.description = description;
    }

    public StepResponseDto() {}

    public static StepResponseDto from(Step step) {
        return new StepResponseDto(step.getId(), step.getStep(), step.getDescription());
    }
}
