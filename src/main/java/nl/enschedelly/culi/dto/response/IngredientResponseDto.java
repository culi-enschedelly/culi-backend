package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.Ingredient;

@Getter
@Setter
public class IngredientResponseDto {

    private Long id;

    private String name;

    public IngredientResponseDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public IngredientResponseDto() {}

    public static IngredientResponseDto from(Ingredient ingredient) {
        return new IngredientResponseDto(ingredient.getId(), ingredient.getName());
    }
}
