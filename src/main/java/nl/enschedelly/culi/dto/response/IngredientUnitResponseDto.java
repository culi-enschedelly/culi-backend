package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.IngredientUnit;

@Getter
@Setter
public class IngredientUnitResponseDto {

    private Long id;

    private String unit;

    public IngredientUnitResponseDto(Long id, String unit) {
        this.id = id;
        this.unit = unit;
    }

    public IngredientUnitResponseDto() {}

    public static IngredientUnitResponseDto from(IngredientUnit unit) {
        return new IngredientUnitResponseDto(unit.getId(), unit.getUnit());
    }
}
