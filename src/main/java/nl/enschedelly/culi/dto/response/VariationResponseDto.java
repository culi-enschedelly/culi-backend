package nl.enschedelly.culi.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.enschedelly.culi.model.Variation;

@Getter
@Setter
public class VariationResponseDto {

    private Long id;

    private String description;

    public VariationResponseDto(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public VariationResponseDto() {}

    public static VariationResponseDto from(Variation variation) {
        return new VariationResponseDto(variation.getId(), variation.getDescription());
    }
}
