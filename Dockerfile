FROM eclipse-temurin:17-jre

WORKDIR /app

# Copy the JAR from the builder
COPY ./target/*.jar ./app.jar

# Create a non-root user
RUN groupadd -r java && useradd -g java javauser
USER javauser

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "./app.jar"]
